package Compiler.Scanner;

/**
  * Provides an enum defining the types of tokens our CMinusScanner can have.
  * 
  * @author James Osborne and Jeremy Tiberg
  * File: TokenType.java
  * Created:  29 Jan 2018
  * Description: This enum covers all the different tokens our lexer can parse.
  **/

public enum TokenType {
    DEFAULT_TOKEN_TYPE, //Used as a default value for an unassigned token
    INT_TOKEN,
    IF_TOKEN,
    ELSE_TOKEN,
    RETURN_TOKEN,
    VOID_TOKEN,
    WHILE_TOKEN,
    PLUS_TOKEN,
    MINUS_TOKEN,
    MULTIPLY_TOKEN,
    DIVIDE_TOKEN,
    LESS_TOKEN,
    LESS_EQ_TOKEN,
    GREATER_TOKEN,
    GREATER_EQ_TOKEN,
    EQUIVALENT_TOKEN,
    NOT_EQUIVALENT_TOKEN,
    ASSIGN_TOKEN,
    SEMICOLON_TOKEN,
    COMMA_TOKEN,
    OPEN_PAREN_TOKEN,
    CLOSE_PAREN_TOKEN,
    OPEN_BRACKET_TOKEN,
    CLOSE_BRACKET_TOKEN,
    OPEN_CURLY_TOKEN,
    CLOSE_CURLY_TOKEN,
    OPEN_COMMENT_TOKEN,
    CLOSE_COMMENT_TOKEN,
    ID_TOKEN,
    NUM_TOKEN,
    EOF_TOKEN,
    ERROR_TOKEN
}
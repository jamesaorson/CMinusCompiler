package Compiler.Scanner;

import static Compiler.Scanner.StateType.*;
import static Compiler.Scanner.TokenType.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
  * Provides a CMinusSyntaxParser object.
  *
  * @author James Osborne and Jeremy Tiberg
  * File: CMinusSyntaxParser.java
  * Created:  22 Jan 2018
  * Description: This Scanner uses a finite automata, designed from a DFA,
  * reflecting the regex statements which define the C- language.
  **/

public final class CMinusScanner implements IScanner {
    private final char EOF = '\uffff';
    private BufferedReader bufferedReader;
    private String inputFile;
    private StateType currentState;
    private String resultString;
    private String tokenString;
    private Token nextToken;
    private int numComments;
    private int currentLine;
    
    public CMinusScanner(String inputFile) throws IOException {
        resetScanner(inputFile);
    }
    
    @Override
    /**
      * Provides the last found token and readies up the next token
      * 
      * @return the Scanner's currently stored next token of the file
      **/
    public Token getNextToken() throws IOException {
        Token returnToken = this.nextToken;
        
        if (!isEOFToken(this.nextToken) && !isErrorToken(this.nextToken)) {
            this.nextToken = scanToken();
        }
        if (isErrorToken(this.nextToken)) {
            throw new RuntimeException("[" + this.inputFile + "] Error token: '" + this.nextToken.getTokenData() + "' encountered");
        }
        
        return returnToken;
    }

    @Override
    /**
      * Provides the last found token
      * 
      * @return the Scanner's currently stored next token of the file
      **/
    public Token viewNextToken() {
        return this.nextToken;
    }
    
    /**
      * Parses and returns the next token of the input file
      * 
      * @return the next token of the input file
      **/
    private Token scanToken() throws IOException {
        Token returnToken = new Token();
        this.currentState = START;
        this.tokenString = "";
        
        while (this.currentState != DONE) {
            TokenType type;
            char character = readCharacter();
            
            switch (this.currentState) {
                /*Starting out, there are a few cases we must check
                  which will determine what next state the lexer will be in,
                  or if the token was simply found*/
                case START:
                    if (isAlphabetic(character)) {
                        this.currentState = IN_ID;
                    } else if (isDigit(character)) {
                        this.currentState = IN_NUM;
                    } else if (isWhitespace(character)) {
                        this.currentState = START;
                    } else {
                        switch (character) {
                            case '/':
                                this.currentState = IN_DIVIDE;
                                break;
                            case '!':
                                this.currentState = IN_NOT;
                                break;
                            case '=':
                                this.currentState = IN_ASSIGN;
                                break;
                            case '<':
                                this.currentState = IN_LT;
                                break;
                            case '>':
                                this.currentState = IN_GT;
                                break;
                            case '+':
                                setSingularCharacterToken(returnToken,
                                                          PLUS_TOKEN,
                                                          character);
                                break;
                            case '-':
                                setSingularCharacterToken(returnToken,
                                                          MINUS_TOKEN,
                                                          character);
                                break;
                            case '*':
                                setSingularCharacterToken(returnToken,
                                                          MULTIPLY_TOKEN,
                                                          character);
                                break;
                            case ';':
                                setSingularCharacterToken(returnToken,
                                                          SEMICOLON_TOKEN,
                                                          character);
                                break;
                            case ',':
                                setSingularCharacterToken(returnToken,
                                                          COMMA_TOKEN,
                                                          character);
                                break;
                            case '(':
                                setSingularCharacterToken(returnToken,
                                                          OPEN_PAREN_TOKEN,
                                                          character);
                                break;
                            case ')':
                                setSingularCharacterToken(returnToken,
                                                          CLOSE_PAREN_TOKEN,
                                                          character);
                                break;
                            case '[':
                                setSingularCharacterToken(returnToken,
                                                          OPEN_BRACKET_TOKEN,
                                                          character);
                                break;
                            case ']':
                                setSingularCharacterToken(returnToken,
                                                          CLOSE_BRACKET_TOKEN,
                                                          character);
                                break;
                            case '{':
                                setSingularCharacterToken(returnToken,
                                                          OPEN_CURLY_TOKEN,
                                                          character);
                                break;
                            case '}':
                                setSingularCharacterToken(returnToken,
                                                          CLOSE_CURLY_TOKEN,
                                                          character);
                                break;
                            case EOF:
                                setSingularCharacterToken(returnToken,
                                                          EOF_TOKEN,
                                                          character);
                                break;
                            default:
                                setSingularCharacterToken(returnToken,
                                                          ERROR_TOKEN,
                                                          character);
                                break;
                        }
                    }
                    
                    /*If the current character is not whitespace,
                      the character must be appended to the tokenString*/
                    if (!isWhitespace(character)) {
                        this.tokenString += character;
                    }
                    break;
                case IN_ID:
                    /*If a non-alphabetic character is found,
                      the token is either complete or errored*/
                    if (isAlphabetic(character)) {
                        this.tokenString += character;
                    } else {
                        /*Since the next character was invalid for an ID,
                          the character must be unread*/
                        unreadCharacter();
                        this.currentState = DONE;
                        
                        switch (tokenString) {
                            case "int":
                                type = INT_TOKEN;
                                break;
                            case "if":
                                type = IF_TOKEN;
                                break;
                            case "else":
                                type = ELSE_TOKEN;
                                break;
                            case "return":
                                type = RETURN_TOKEN;
                                break;
                            case "void":
                                type = VOID_TOKEN;
                                break;
                            case "while":
                                type = WHILE_TOKEN;
                                break;
                            default:
                                //ID tokens should not contain numbers
                                if (isDigit(character)) {                                   
                                    character = readCharacter();
                                    
                                    /*Error correction to read the entirety
                                      of the errored token and recover*/
                                    while (isAlphabeticOrDigit(character)) {
                                        this.tokenString += character;
                                        character = readCharacter();
                                    }
                                     
                                    unreadCharacter();
                                    type = ERROR_TOKEN;
                                } else {
                                    type = ID_TOKEN;
                                }
                                
                                break;
                        }
                        
                        returnToken.setTokenFields(type, this.tokenString, this.currentLine);
                    }
                    
                    break;
                case IN_NUM:
                    /*If a non-digit character is found,
                      the token is either complete or errored*/
                    if (isDigit(character)) {
                        this.tokenString += character;
                    }else {
                        /*Since the next character was invalid for a num,
                          the character must be unread*/
                        unreadCharacter();
                        this.currentState = DONE;
                        
                        //Num tokens should not contain letters
                        if (isAlphabetic(character)) {
                            character = readCharacter();
                            
                            /*Error correction to read the entirety
                              of the errored token and recover*/
                            while (isAlphabeticOrDigit(character)) {
                                this.tokenString += character;
                                character = readCharacter();
                            }
                            
                            unreadCharacter();
                            type = ERROR_TOKEN;
                        } else {
                            type = NUM_TOKEN;
                        }
                        
                        returnToken.setTokenFields(type, this.tokenString, this.currentLine);
                    }
                    
                    break;
                case IN_DIVIDE:
                    setConditionalToken(returnToken,
                                        character,
                                        DIVIDE_TOKEN,
                                        '*',
                                        OPEN_COMMENT_TOKEN);
                    if (character == '*') {
                            this.tokenString += character;
                        this.currentState = IN_COMMENT;
                        ++this.numComments;
                    } else {
                        unreadCharacter();
                        type = DIVIDE_TOKEN;
                    }

                    break;
                case IN_COMMENT:
                    while (this.currentState != START
                           && this.currentState != DONE) {
                        character = readCharacter();
                        if (character == '*') {
                            character = readCharacter();
                            if (character == '/') {
                                --this.numComments;
                                if (this.numComments == 0) {
                                    this.currentState = START;
                                }
                            } else {
                                unreadCharacter();
                            }
                        } else if (character == '/') {
                            character = readCharacter();
                            if (character == '*') {
                                ++this.numComments;
                            } else {
                                unreadCharacter();
                            }
                        } else if (character == EOF) {
                            this.currentState = DONE;
                            returnToken.setTokenFields(EOF_TOKEN, EOF, this.currentLine);
                        }
                    }
                    this.tokenString = "";
                    break;
                case IN_NOT:
                    this.currentState = DONE;
                    this.tokenString += character;
                    if (character == '=') {
                        returnToken.setTokenFields(NOT_EQUIVALENT_TOKEN, this.tokenString, this.currentLine);
                    } else {
                        returnToken.setTokenFields(NOT_EQUIVALENT_TOKEN, this.tokenString, this.currentLine);
                    }
                    break;
                case IN_ASSIGN:
                    setConditionalToken(returnToken,
                                        character,
                                        ASSIGN_TOKEN,
                                        '=',
                                        EQUIVALENT_TOKEN);
                    break;
                case IN_LT:
                    setConditionalToken(returnToken,
                                        character,
                                        LESS_TOKEN,
                                        '=', 
                                        LESS_EQ_TOKEN);
                    break;
                case IN_GT:
                    setConditionalToken(returnToken,
                                        character,
                                        GREATER_TOKEN, 
                                        '=',
                                        GREATER_EQ_TOKEN);
                    break;
            }
            
            if (character == '\n') {
                this.currentLine++;
            }
        }
        
        Object tokenData = returnToken.getTokenData();
        this.resultString += returnToken.getTokenType().toString() + "\n\t"; 
        
        if (tokenData instanceof Integer) {
            this.resultString += Integer.parseInt(tokenData.toString()) + "\n";
        } else {
            this.resultString += tokenData.toString() + "\n";
        }
        
        return returnToken;
    } 
    
    /**
      * Provides the next character of the input file
      * 
      * @return the next character of the input file
      **/
    private char readCharacter() throws IOException {
        //This marks a point in the reader to move back to for unreading
        this.bufferedReader.mark(1);
        
        return (char) this.bufferedReader.read();
    }
    
    /**
      * Reverses the file pointer by one character position
      **/
    private void unreadCharacter() throws IOException {
        //This moves the reader back to the mark made in readCharacter();
        this.bufferedReader.reset();
    }
    
    /**
      * Appends provided character to the current tokenString,
      * sets current state to DONE, and sets fields of provided
      * token with provided tokenType and new tokenString.
      * 
      * @param token Token to be assigned to
      * @param tokenType Type to assign to token
      * @param character Character to append to tokenString
      **/
    private void setSingularCharacterToken(Token token, 
                                           TokenType tokenType, 
                                           char character) {        
        this.tokenString += character;
        this.currentState = DONE;
        token.setTokenFields(tokenType, this.tokenString, this.currentLine);
    }
    /**
      * Appends provided character to the current tokenString,
      * sets current state to DONE, and sets fields of provided
      * token with provided tokenType and new tokenString. The
      * TokenType of the passed token is determined by the conditionalCharacter.
      * 
      * @param token Token to be assigned to
      * @param currentCharacter gets checked if equal to conditionalCharacter
      * @param currentTokenType token type to be assigned if condition fails
      * @param conditionalCharacter character checks against this character
      * @param conditionalTokenType token type to be assigned
      * if condition succeeds
      **/
    private void setConditionalToken(Token token,
                                     char currentCharacter,
                                     TokenType currentTokenType,
                                     char conditionalCharacter,
                                     TokenType conditionalTokenType) throws IOException {
        TokenType type;
        
        if (currentCharacter == conditionalCharacter) {
            this.tokenString += currentCharacter;
            type = conditionalTokenType;
        } else {
            unreadCharacter();
            type = currentTokenType;
        }

        this.currentState = DONE;
        token.setTokenFields(type, this.tokenString, this.currentLine);
    }
    
    /**
      * Checks if token is an EOF token
      * 
      * @param token Token to be checked
      * @return result of whether or not the token is an EOF_TOKEN
      **/
    private boolean isEOFToken(Token token) {
        if (token == null) {
            return false;
        }
        
        return token.getTokenType() == EOF_TOKEN;
    }
    
    /**
      * Checks if token is an Error token
      * 
      * @param token Token to be checked
      * @return result of whether or not the token is an ERROR_TOKEN
      **/
    private boolean isErrorToken(Token token) {
        if (token == null) {
            return false;
        }
        
        return token.getTokenType() == ERROR_TOKEN;
    }
    
    /**
      * Checks if character is whitespace
      * 
      * @param character Character to check
      * @return result of whether or not the character is whitespace
      **/
    private boolean isWhitespace(char character) {
        return Character.isWhitespace(character);
    }
    
    /**
      * Checks if character is alphabetic
      * 
      * @param character Character to check
      * @return result of whether or not the character is alphabetic
      **/
    private boolean isAlphabetic(char character) {
        return Character.isAlphabetic(character);
    }
    
    /**
      * Checks if character is a digit
      * 
      * @param character Character to check
      * @return result of whether or not the character is a digit
      **/
    private boolean isDigit(char character) {
        return Character.isDigit(character);
    }
    
    /**
      * Checks if character is alphabetic or a digit
      * 
      * @param character Character to check
      * @return result of whether or not the character is alphabetic or a digit
      **/
    private boolean isAlphabeticOrDigit(char character) {
        return isAlphabetic(character) || isDigit(character);
    }
    
    /**
      * Outputs result of lexer to file of provided name
      * and prints result to console
      * 
      * @param outputFile name of file to output result of lexer to
      **/
    public void writeToOutputFile(String outputFile) throws IOException {
        /*By wrapping this in a try, this creates the writer
          in a limited scope and will automatically closing
          out the writer and writing the contents to the file*/
        try (BufferedWriter bufferedWriter = 
                new BufferedWriter(new FileWriter(outputFile))) {
            bufferedWriter.write(this.resultString);
        }
        
        System.out.println("WRITTEN TO FILE:\n\n" + this.resultString);
    }
    
    /**
      * Opens the BufferedWriter with the new input file and resets class fields
      * 
      * @param inputFile name of input file to parse
      **/
    public void resetScanner(String inputFile) throws IOException {
        this.bufferedReader = new BufferedReader(new FileReader(inputFile));
        this.bufferedReader.mark(1);
        this.resultString = "";
        this.nextToken = null;
        this.getNextToken();
        this.numComments = 0;
        this.currentLine = 1;
        this.inputFile = inputFile;
    }
}

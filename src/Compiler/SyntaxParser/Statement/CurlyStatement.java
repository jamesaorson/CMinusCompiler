package Compiler.SyntaxParser.Statement;

import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;
import java.util.ArrayList;
import java.util.HashMap;
import lowlevel.*;
import Compiler.SyntaxParser.Decl.*;
import static lowlevel.Operand.OperandType.BLOCK;
import static lowlevel.Operation.OperationType.JMP;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: CurlyStatement.java
  **/
public class CurlyStatement extends Statement {
    private ArrayList<VarDecl> varDeclarations;
    private ArrayList<Statement> statementList;
    
    public CurlyStatement() {
        this(new ArrayList<VarDecl>(), new ArrayList<Statement>());
    }
    
    public CurlyStatement(ArrayList<VarDecl> varDeclarations, ArrayList<Statement> statementList) {
        this.varDeclarations = varDeclarations;
        this.statementList = statementList;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Curly Statement {");
        
        printHelper(childIndent, "{");
        for (VarDecl varDecl : this.varDeclarations) {
            varDecl.print(childIndent);
        }
        for (Statement statement : this.statementList) {
            statement.print(childIndent);
        }
        printHelper(childIndent, "}");
        
        printHelper(indent, "}");
    }
    
    @Override
    public void genLLCode(Function function) {
        genLLCode(function, null, null);
    }
    
    @Override
    public void genLLCode(Function function, BasicBlock returnBlock, BasicBlock basicBlock) {
        HashMap funcSymbolTable = function.getTable();
        if (basicBlock == null) {
            basicBlock = new BasicBlock(function);
            basicBlock.setNextBlock(function.getCurrBlock().getNextBlock());
            function.appendToCurrentBlock(basicBlock);
            function.setCurrBlock(basicBlock);
        }
        
        for (VarDecl varDecl : varDeclarations) {
            Data data = (Data) varDecl.genLLCode(function.getTable());
            
            funcSymbolTable.put(data.getName(), function.getNewRegNum());
        }
        
        for (Statement statement : statementList) {
            statement.genLLCode(function, returnBlock);
        }
    }
}

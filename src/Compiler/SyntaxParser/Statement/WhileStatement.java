package Compiler.SyntaxParser.Statement;

import Compiler.SyntaxParser.Statement.Statement;
import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;
import Compiler.SyntaxParser.Expression.*;
import lowlevel.BasicBlock;
import lowlevel.Function;
import lowlevel.Operand;
import static lowlevel.Operand.OperandType.*;
import lowlevel.Operation;
import static lowlevel.Operation.OperationType.*;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: WhileStatement.java
  **/
public class WhileStatement extends Statement {
    private Expression whileExpression;
    private Statement whileStatement;
    
    public WhileStatement() {
        this(null, null);
    }
    
    public WhileStatement(Expression whileExpression, Statement whileStatement) {
        this.whileExpression = whileExpression;
        this.whileStatement = whileStatement;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "While Statement {");
        
        printHelper(childIndent, "while");
        printHelper(childIndent, "(");
        this.whileExpression.print(childIndent);
        printHelper(childIndent, ")");
        this.whileStatement.print(childIndent);
        
        printHelper(indent, "}");
    }
    
    public void genLLCode(Function function) {
        genLLCode(function, null);
    }
    
    public void genLLCode(Function function, BasicBlock returnBlock) {
        genLLCode(function, returnBlock, null);
    }
    
    public void genLLCode(Function function, BasicBlock returnBlock, BasicBlock basicBlock) {
        BasicBlock currBlock = function.getCurrBlock();
        BasicBlock thenBlock = new BasicBlock(function);
        BasicBlock postBlock = new BasicBlock(function);
        
        //Call gen code on the expression
        Operation whileOperation = whileExpression.genLLCode(function);
        Operation branchOperation = new Operation(BEQ, currBlock);
        currBlock.appendOper(branchOperation);
        
        if (whileExpression instanceof CallExpression) {
            branchOperation.setSrcOperand(0, new Operand(MACRO, "RetReg"));
        } else {
            branchOperation.setSrcOperand(0, whileOperation.getDestOperand(0));
        }
        branchOperation.setSrcOperand(1, new Operand(INTEGER, 0));
        branchOperation.setSrcOperand(2, new Operand(BLOCK, postBlock.getBlockNum()));
        
        thenBlock.setNextBlock(currBlock.getNextBlock());
        function.appendToCurrentBlock(thenBlock);
        function.setCurrBlock(thenBlock);
        currBlock = thenBlock;
        
        whileStatement.genLLCode(function, null, currBlock);
        whileOperation = whileExpression.genLLCode(function);
        branchOperation = new Operation(BNE, thenBlock);
        currBlock.appendOper(branchOperation);
        
        if (whileExpression instanceof CallExpression) {
            branchOperation.setSrcOperand(0, new Operand(MACRO, "RetReg"));
        } else {
            branchOperation.setSrcOperand(0, whileOperation.getDestOperand(0));
        }
        branchOperation.setSrcOperand(1, new Operand(INTEGER, 0));
        branchOperation.setSrcOperand(2, new Operand(BLOCK, thenBlock.getBlockNum()));
        
        function.appendToCurrentBlock(postBlock);
        function.setCurrBlock(postBlock);
    }
}

package Compiler.SyntaxParser.Decl;

import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;
import java.util.HashMap;
import lowlevel.*;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: Decl.java
  **/
public class Decl {
    protected String typeSpecifier;
    protected String id;
    private Decl declaration;
    
    public Decl() {
        this.typeSpecifier = null;
        this.id = null;
        this.declaration = null;
    }
    
    public Decl(String typeSpecifier, String id, Decl declaration) {
        this.typeSpecifier = typeSpecifier;
        this.id = id;
        this.declaration = declaration;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Decl {");
        
        printHelper(childIndent, this.typeSpecifier);
        printHelper(childIndent, this.id);
        this.declaration.print(childIndent);
        
        printHelper(indent, "}");
    }
    
    public CodeItem genLLCode(HashMap symbolTable) {
        throw new LowLevelException("Should not call genLLCode on generic decl");
    }
}

package Compiler.SyntaxParser.Expression;

import lowlevel.*;
import static lowlevel.Operand.OperandType.*;
import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;
import lowlevel.Operation.OperationType;
import static lowlevel.Operation.OperationType.*;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: BinopExpression.java
  **/
public class BinopExpression extends Expression {
    private Expression leftHandSide;
    private String binop;
    private Expression rightHandSide;
    
    public BinopExpression(Expression leftHandSide) {
        this(leftHandSide, null, null);
    }
    
    public BinopExpression(Expression leftHandSide,
                           String binop, 
                           Expression rightHandSide) {
        this.leftHandSide = leftHandSide;
        this.binop = binop;
        this.rightHandSide = rightHandSide;
    }
    
    public void setBinop(String binop) {
        this.binop = binop;
    }
    
    public void setRightHandSide(Expression rightHandSide) {
        this.rightHandSide = rightHandSide;
    }
    
    public Expression getRightHandSide() {
        return this.rightHandSide;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Binop Expression {");
        
        this.leftHandSide.print(childIndent);
        if (this.rightHandSide != null) {
            printHelper(childIndent, this.binop);
            this.rightHandSide.print(childIndent);
        }
        
        printHelper(indent, "}");
    }
    
    @Override
    public Operation genLLCode(Function function) {
        OperationType operationType = null;
        
        switch (binop) {
            case "+":
                operationType = ADD_I;
                break;
            case "-":
                operationType = SUB_I;
                break;
            case "*":
                operationType = MUL_I;
                break;
            case "/":
                operationType = DIV_I;
                break;
            case "<":
                operationType = LT;
                break;
            case "<=":
                operationType = LTE;
                break;
            case ">":
                operationType = GT;
                break;
            case ">=":
                operationType = GTE;
                break;
            case "==":
                operationType = EQUAL;
                break;
            case "!=":
                operationType = NOT_EQUAL;
                break;
            default:
                throw new LowLevelException("Invalid binop: " + binop);
        }
        
        BasicBlock currBlock = function.getCurrBlock();
        Operation operation = new Operation(operationType, function.getCurrBlock());
        operation.setDestOperand(0, new Operand(REGISTER, function.getNewRegNum()));
        
        Operation leftOperation = leftHandSide.genLLCode(function);
        if (leftHandSide instanceof CallExpression) {
            currBlock.appendOper(leftOperation);
            operation.setSrcOperand(0, new Operand(MACRO, "RetReg"));
        } else {
            if (leftOperation.getSrcOperand(0).getType() == STRING) {
                Operation storeOperation = new Operation(STORE_I, currBlock);
                storeOperation.setSrcOperand(0, new Operand(REGISTER, function.getNewRegNum()));
                storeOperation.setSrcOperand(1, new Operand(STRING, leftOperation.getSrcOperand(0).getValue()));
                storeOperation.setSrcOperand(2, new Operand(INTEGER, 0));
                currBlock.appendOper(storeOperation);
                
                operation.setSrcOperand(0, new Operand(REGISTER, function.getMaxRegNum()));
            } else {
                operation.setSrcOperand(0, leftOperation.getDestOperand(0));
            }
        }
        
        Operation rightOperation = rightHandSide.genLLCode(function);
        if (rightHandSide instanceof CallExpression) {
            currBlock.appendOper(rightOperation);
            operation.setSrcOperand(0, new Operand(MACRO, "RetReg"));
        } else {
            if (rightOperation.getSrcOperand(0).getType() == STRING) {
                Operation storeOperation = new Operation(STORE_I, currBlock);
                storeOperation.setSrcOperand(0, new Operand(REGISTER, function.getNewRegNum()));
                storeOperation.setSrcOperand(1, new Operand(STRING, rightOperation.getSrcOperand(0).getValue()));
                storeOperation.setSrcOperand(2, new Operand(INTEGER, 0));
                currBlock.appendOper(storeOperation);

                operation.setSrcOperand(1, new Operand(REGISTER, function.getMaxRegNum()));
            }
            operation.setSrcOperand(1, rightOperation.getDestOperand(0));
        }
        
        currBlock.appendOper(operation);
        
        return operation;
    }
}

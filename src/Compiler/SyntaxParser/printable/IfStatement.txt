package Compiler.SyntaxParser.Statement;

import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;
import Compiler.SyntaxParser.Expression.Expression;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: IfStatement.java
  **/
public class IfStatement extends Statement {
    private Expression ifExpression;
    private Statement ifStatement;
    private Statement elseStatement;
    
    public IfStatement() {
        this(null, null, null);
    }
    
    public IfStatement(Expression ifExpression, Statement ifStatement) {
        this(ifExpression, ifStatement, null);
    }
    
    public IfStatement(Expression ifExpression, Statement ifStatement, Statement elseStatement) {
        this.ifExpression = ifExpression;
        this.ifStatement = ifStatement;
        this.elseStatement = elseStatement;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "If Statement {");
        
        printHelper(childIndent, "if");
        printHelper(childIndent, "(");
        this.ifExpression.print(childIndent);
        printHelper(childIndent, ")");
        this.ifStatement.print(childIndent);
        if (this.elseStatement != null) {
            printHelper(childIndent, "else");
            this.elseStatement.print(childIndent);
        }
        
        printHelper(indent, "}");
    }
}

package Compiler.SyntaxParser.Statement;

import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;
import Compiler.SyntaxParser.Expression.Expression;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: ExpressionStatement.java
  **/
public class ExpressionStatement extends Statement {
    private Expression expression;
    
    public ExpressionStatement() {
        this(null);
    }
    
    public ExpressionStatement(Expression expression) {
        this.expression = expression;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Expression Statement {");
        
        if (this.expression != null) {
            this.expression.print(childIndent);
        }
        printHelper(childIndent, ";");
        
        printHelper(indent, "}");
    }
}
